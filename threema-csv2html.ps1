﻿###
# Allgemeine Parameter
param(
	[Parameter(Mandatory)][ValidatePattern("^listc(ontacts)?$|^listg(roups)?$|^help$|.+\.csv$")][string]$Command_or_ChatFile,
	[string]$myOwnName,
	[string]$myOwnThreemaID,
	[string]$SenderID,
	[string]$FirstDate,
	[string]$LastDate,
	[switch]$ShowThreemaIDs,
	[switch]$ShowAllIdentities,
	[switch]$LinkQuotes,
	[switch]$RealFilenames,
	[switch]$WebStyle,
	[switch]$DarkMode,
	[string]$CSS
)
[string]$my_version = "3.0"
##################################################################################################################
[array]$HelpText='erstellt 2020 von jnL, weiterentwickelt ab 2023 von Bawal, Web Style von Curve'
$HelpText+=''
$HelpText+='threema-csv2html.ps1 ListC[ontacts] | ListG[roups] | Help | <File>'
$HelpText+='	[-myOwnName "<Name>"] [-myOwnThreemaID "<ID>"] [-SenderID "<ID>"]'
$HelpText+='	[-FirstDate "<Date>"] [-LastDate "<Date>"]'
$HelpText+='	[-ShowThreemaIDs] [-ShowAllIdentities] [-LinkQuotes] [-RealFilenames]'
$HelpText+='	[-WebStyle] [-DarkMode] [-CSS "<File>"]'
$HelpText+=''
$HelpText+='ListC[ontacts]'
$HelpText+='	zeige alle Threema-Kontakte und die entsprechenden Chat-Dateien'
$HelpText+='ListG[roups]'
$HelpText+='	zeige alle Gruppen-Chats und die entsprechenden Chat-Dateien'
$HelpText+='<File>'
$HelpText+='	konvertiere die angegebene(n) Chat-Datei(en) in lesbares HTML-Format'
$HelpText+='	Platzhalter erlaubt, <File> muss mit ".csv" enden'
$HelpText+='Help'
$HelpText+='	zeige diesen Hilfetext an'
$HelpText+='-myOwnName "<Name>"'
$HelpText+='	benutze <Name> als eigener Threema-Name in Chats (z.B. "Erika Mustermann")'
$HelpText+='-myOwnThreemaID "<ID>"'
$HelpText+='	benutze <ID> als eigene Threema-ID in Chats (z.B. "ABCDEFGH")'
$HelpText+='-SenderID "<ID>"'
$HelpText+='	konvertiere den Gruppenchat aus der Sicht des Mitglieds mit der Threema-ID <ID> (Beiträge stehen somit auf der rechten Seite des Chats)'
$HelpText+='	Achtung: funktioniert nur bei Gruppenchats!'
$HelpText+='-FirstDate "<Date>"'
$HelpText+='	konvertiere den Chat erst ab Datum <Date>'
$HelpText+='-LastDate "<Date>"'
$HelpText+='	konvertiere den Chat bis zum Datum <Date>'
$HelpText+='-ShowThreemaIDs'
$HelpText+='	verwende auch die Threema-IDs bei Absendern'
$HelpText+='-ShowAllIdentities'
$HelpText+='	bei Gruppenchats: verwende auch den eigenen Absender (Name/Threema-ID) bei gesendeten Nachrichten'
$HelpText+='	bei 1:1-Chats: verwende Absender (Name/Threema-ID) bei empfangenen und gesendeten Nachrichten'
$HelpText+='-LinkQuotes'
$HelpText+='	erzeuge klickbare Zitate (experimentell!)'
$HelpText+='-RealFilenames'
$HelpText+='	falls vorhanden, benutze die echten Dateinamen bei (Medien-)Dateien'
$HelpText+='-WebStyle'
$HelpText+='	erzeuge HTML im Stil von Threema Web'
$HelpText+='-DarkMode'
$HelpText+='	erzeuge HTML in dunklen Farben'
$HelpText+='-CSS "<File>"'
$HelpText+='	verweise auf eine externe CSS-Datei, anstatt einen internen Style in die HTML einzufügen'
$HelpText+=''
##################################################################################################################


# Funktion zum Konvertieren der Sekunden-Zeitstempel
Function ConvertUnixTime ([string]$msg_timestamp_unix, [bool]$fulldatetime){
	$msg_timestamp_unix = $msg_timestamp_unix.Substring(0, 10)
	If ($fulldatetime -eq $false){
	   (([datetime]'1/1/1970 01:00').AddSeconds($msg_timestamp_unix)).ToString("dd.MM.yyyy")
	} Else {
	   (([datetime]'1/1/1970 01:00').AddSeconds($msg_timestamp_unix)).ToString("dd.MM.yyyy, HH:mm:ss")
	}
}

# Funktion zum Auslesen des Kontaktnamens
Function GetContactName ($ask_chat_id, [bool]$group_not_user, [string]$memyself_word){
	If ($group_not_user){ # Test auf Gruppe
		For ([long]$i=0; $i -lt $csv_content_groups.Count; $i++){
			If ($csv_content_groups[$i].group_uid -eq $ask_chat_id -or $csv_content_groups[$i].id -eq $ask_chat_id){
				# Gebe Gruppenname zurück
				Return $csv_content_groups[$i].groupname
			}
		}
	} Else { # Einzelchat oder Erwähnung
		# Alle in einer Gruppe
		If ($ask_chat_id -eq "@@@@@@@@"){Return "Alle"}
		# keine ID -> man selbst
		If ($ask_chat_id -eq "") {Return $memyself_word} # geht schneller
		#
		For ([long]$i=0; $i -lt $csv_content_contacts.Count; $i++){
			If ($csv_content_contacts[$i].identity_id -eq $ask_chat_id -or $csv_content_contacts[$i].identity -eq $ask_chat_id){
				#
				# Spezial-Aufruf, um beim 1:1-Chat die Threeme-ID des Chat-Partners zu bekommen!
				If ($memyself_word -eq "&#128077;"){
					Return $csv_content_contacts[$i].identity
				}
				#
				[string]$tmp_fullname = $csv_content_contacts[$i].firstname + " " + $csv_content_contacts[$i].lastname
				$tmp_fullname = $tmp_fullname.Trim(" ")
				If ($tmp_fullname -eq ""){
					$tmp_fullname = $csv_content_contacts[$i].nick_name
					$tmp_fullname = $tmp_fullname.Trim(" ")
					If ($tmp_fullname -eq ""){
						$tmp_fullname = $ask_chat_id
					}
				}
				Return $tmp_fullname
			}
		}
		# Kein Treffer? Gebe die übergebene Variable zurück (für eigene Zitate, ...)
		Return $memyself_word
	}
}

# Funktion zur Erkennung des Datei-Formats bzw. des Suffix
Function GetFileSuffix ($my_Filename){
	If (Test-Path -Path ($my_Filename)){
		Switch -Wildcard ([string](Get-Content $my_Filename -Encoding Byte -ReadCount 1 -TotalCount 4)){
			"255 216 255*" { # JPG
				Return ".jpg"
			}
			"137 80 78 71" { # PNG
				Return ".png"
			}
			"71 73 70 56" { # GIF
				Return ".gif"
			}
			default {
				Return ""
			}
		}
	} Else {
		Return ""
	}
}

# Funktion zum Verarbeiten der Nachricht in die HTML-Datei
Function WriteMessage ($msg_content){
	# allgemeine, temporäre Parameter in dieser Funktion
	[array]$tmp_content = $null
	[string]$tmp_content_from_user = ""
	[string]$tmp_msg_content = ""
	[string]$tmp_group_user_id = ""
	[string]$tmp_group_user_name = ""
	[string]$tmp_real_filename = ""
	
	If (-Not $msg_is_quote) {
		$msg_content.body = $msg_content.body.Replace("<", "&lt;") # Ersetze < Zeichen, um den HTML-Code nicht zu zerstören
		$msg_content.body = $msg_content.body -replace 'https?:\/\/[^ \s]+', '<a href="$0" target="_blank">$0</a>' # Text-URLs in echte Links umwandeln
	}
	
	If ($isGroup){ # Test auf Gruppe
		[string]$tmp_filename = "group_message_media_"+$msg_content.uid # Setze mögliche Medien-ID
		[string]$tmp_filename_thumbnail = "group_message_thumbnail_"+$msg_content.uid # Setze mögliche Medien-ID zum Vorschaubild
	} Else {
		[string]$tmp_filename = "message_media_"+$msg_content.uid # Setze mögliche Medien-ID für Einzelchat (auch Kanal?)
		[string]$tmp_filename_thumbnail = "message_thumbnail_"+$msg_content.uid # Setze mögliche Medien-ID für Einzelchat (auch Kanal?) zum Vorschaubild
	}
	# neuer Versuch, die Namensfindung zu Verallgemeinern
	If ($msg_is_status){ # kein Name notwendig
		$tmp_content_from_user = ""
	} Else {
		# Gruppen-Chat
		If ($isGroup){
			# eingehende Nachricht
			If ($msg_content.isoutbox -eq 0){
				$tmp_group_user_id = $msg_content.identity # Hole die ID des Senders
				$tmp_group_user_name = GetContactName $tmp_group_user_id $false "<i>Unbekannt</i>" # Setze den Namen zur Sender-ID
			# ausgehende Nachricht
			} Else {
				If ($ShowAllIdentities -Or $msg_is_quote){
					$tmp_group_user_id = $msg_content.identity # Hole die ID des Senders
					If ($tmp_group_user_id -eq ""){ # nach Erfahrung bei ausgehenden Nachrichten immer leer!!
						$tmp_group_user_id = $myOwnThreemaID
						$tmp_group_user_name = $myOwnName
					} Else {
						$tmp_group_user_name = GetContactName $tmp_group_user_id $false $myOwnName # Setze den Namen zur Sender-ID
					}
				} Else {
					$tmp_group_user_id = ""
					$tmp_group_user_name = ""
				}
			}
			If ($tmp_group_user_name -eq "" -And $tmp_group_user_id -eq ""){
				$tmp_content_from_user = ""
			} Else {
				If ($ShowThreemaIDs -And $tmp_group_user_id -ne ""){
					$tmp_content_from_user = "`t<p class=""identity"">"+$tmp_group_user_name+"<ident-id> ("+$tmp_group_user_id+")</ident-id></p>`n"
				} Else {
					$tmp_content_from_user = "`t<p class=""identity"">"+$tmp_group_user_name+"</p>`n"
				}
			}
		# Einzel-Chat
		} Else {
			If ($ShowAllIdentities -Or $msg_is_quote){
				# eingehende Nachricht
				If ($msg_content.isoutbox -eq 0){
					[string]$tmp_identity_name = $chat_partner_name
					[string]$tmp_identity_id = $chat_partner_ID
				# ausgehende Nachricht
				} Else {
					[string]$tmp_identity_name = $myOwnName
					[string]$tmp_identity_id = $myOwnThreemaID
				}
				If ($ShowThreemaIDs -And $tmp_identity_id -ne ""){
					$tmp_content_from_user = "`t<p class=""identity"">"+$tmp_identity_name+"<ident-id> ("+$tmp_identity_id+")</ident-id></p>`n"
				} Else {
					$tmp_content_from_user = "`t<p class=""identity"">"+$tmp_identity_name+"</p>`n"
				}
			} Else {
				$tmp_content_from_user = "" # im 1:1-Chat gibt es normalerweise keine Benutzerangaben
			}
		}
	}
	# Verarbeitung je nach Nachrichtentyp
	Switch ($msg_content.type){
		"TEXT" {
			# Ersetze Erwähnungen: @[ABCD1234]
			While ($msg_content.body | Select-String -CaseSensitive "@\[........\]"){
				$tmp_at_index = $msg_content.body.IndexOf("@[")
				$tmp_at_full = $msg_content.body.Substring($tmp_at_index, 11)
				$tmp_at_user_id = $msg_content.body.Substring($tmp_at_index + 2, 8)
				$tmp_at_user_name = GetContactName $tmp_at_user_id $false $myOwnName
				If ($tmp_at_user_id -eq "@@@@@@@@" -Or $ShowThreemaIDs -eq $false){
					$msg_content.body = $msg_content.body.Replace($tmp_at_full, "<mark>@" + $tmp_at_user_name + "</mark>")
				} Else {
					$msg_content.body = $msg_content.body.Replace($tmp_at_full, "<mark>@" + $tmp_at_user_name + " (" + $tmp_at_user_id + ")</mark>")
				}
			}
			# Status-Nachricht
			If ($msg_is_status){
				$msg_timestamp_readable = ConvertUnixTime $msg_content.created_at $true
				$tmp_content += "`t<p class=""msg""><span title="""+$msg_timestamp_readable+""">"+$msg_content.body.Replace("`n", "<br/>")+"</span></p>" # benutze Timestamp als MouseOver-Text
			# Zitat (altes Format)
			} ElseIf ($msg_content.body -like "> ????????: *"){
				[string]$tmp_quotecontact_id = $msg_content.body.Substring(2, 8)
				[string]$tmp_quotecontact_name = GetContactName $tmp_quotecontact_id $false $myOwnName
				#
				$tmp_msg_content = "`t<div class=""message quote"">`n"
				$msg_content_split = $msg_content.body.Split("`n")
				#
				If ($ShowThreemaIDs){
					$tmp_msg_content += "`t`t<p class=""identity"">"+$tmp_quotecontact_name+"<ident-id> ("+$tmp_quotecontact_id+")</ident-id></p>`n"
				} Else {
					$tmp_msg_content += "`t`t<p class=""identity"">"+$tmp_quotecontact_name+"</p>`n"
				}
				#
				$tmp_msg_content += "`t`t<p class=""msg"">" # hier fängt die Nachricht an
				$tmp_msg_content += $msg_content_split[0].Replace("> "+$tmp_quotecontact_id+": ", "")
				$tmp_quote_closed = $false
				#
				For ([long]$i=1; $i -lt $msg_content_split.Count; $i++){
					If ($msg_content_split[$i].StartsWith("> ") -eq $true){
						$tmp_msg_content += "<br/>"+$msg_content_split[$i].Substring(2) # Replace("> ", "") wurde ersetzt, weil ">" als Link woanders vorhanden sein könnte!
					} ElseIf ($tmp_quote_closed -And $i -lt $msg_content_split.Count){
						If ($i -lt $msg_content_split.Count){
							$tmp_msg_content += $msg_content_split[$i]+"<br/>"
						} Else {
							$tmp_msg_content += $msg_content_split[$i]
						}
					} Else {
						$tmp_msg_content += "</p>`n`t</div>`n"
						$tmp_msg_content += $tmp_content_from_user+"`t<p class=""msg"">"+$msg_content_split[$i]+"<br/>"
						$tmp_quote_closed = $true
					}
				}
				$tmp_content += $tmp_msg_content + "</p>"
			# ganz normaler Text
			} Else {
				$tmp_content += $tmp_content_from_user+"`t<p class=""msg"">"+$msg_content.body.Replace("`n", "<br/>")+"</p>"
			}
		break}
		#
		"FILE" { # über "IMAGE" gesetzt, damit man mit type-Änderung noch die anderen Switche erreichen kann
			$tmp_real_filename = $msg_content.body.Replace("""", "").Split(",")[4] # echter Dateiname an 5. Position
			[string]$tmp_real_filename_caption = $tmp_real_filename # brauchen wir extra für die Anzeige
			If ($RealFilenames -And $tmp_real_filename){
				$tmp_real_filename = "__" + $tmp_real_filename # Trennzeichen! #
			} Else {
				$tmp_real_filename = ""
			}
			[string]$filemime = $msg_content.body.Split(",")[2] # MIME-Type an 3. Position
			# hier darf ich kein "Switch" mehr benutzen, damit wir $_ verändern können
			If ($filemime -match "audio/") {
				$_ = "AUDIO"
			} ElseIf ($filemime -match "image/") {
				$_ = "IMAGE"
			} ElseIf ($filemime -match "video/") {
				$_ = "VIDEO"
			} Else { # default
				If ($msg_content.caption -like "?*"){
					$tmp_msg_content = "<br/>"+$msg_content.caption.Replace("`n", "<br/>")
				}
				If (Test-Path -Path $tmp_filename){ # Teste ob Datei vorhanden
					Move-Item $tmp_filename $chat_id_folder/$tmp_filename$tmp_real_filename # sieht gruselig aus...muss aber so
					#Remove-Item $tmp_filename_thumbnail 2>$null # eigentlich kein Grund, das Thumbnail zu löschen. uncomment bei Bedarf
				} ElseIf (Test-Path -Path $tmp_filename_thumbnail){ # Teste ob Thumbnail vorhanden
					If ($RealFilenames){
						$tmp_real_filename = GetFileSuffix $tmp_filename_thumbnail
					}		
					Move-Item $tmp_filename_thumbnail $chat_id_folder/$tmp_filename_thumbnail$tmp_real_filename
				}
				If (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename+$tmp_real_filename)){
					$tmp_content = $tmp_content_from_user+"`t<p class=""file""><b>Datei</b>: <a target=""blank"" href="""+$chat_id_folder+"/"+$tmp_filename+$tmp_real_filename+""">"+$tmp_real_filename_caption+"</a><br/>"+$tmp_msg_content+"</p>"
				} ElseIf (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename)){
					$tmp_content = $tmp_content_from_user+"`t<p class=""file""><i>Datei (Datei: "+$chat_id_folder+"/"+$tmp_filename+") nicht gefunden. Gespeichertes Vorschaubild:</i><br/><a target=""blank"" href="""+$chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename+"""><img src="""+$chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename+"""></a><br/>"+$tmp_msg_content+"</p>"
				} Else { # leider weder Datei noch Thumbnail gefunden
					$tmp_content = $tmp_content_from_user+"`t<p class=""file""><i>Datei (Datei: "+$chat_id_folder+"/"+$tmp_filename+") nicht gefunden.</i><br/>"+$tmp_real_filename_caption+"<br/>"+$tmp_msg_content+"</p>"
				}
				break
			}
		}
		#
		"IMAGE" {
			If ($msg_content.caption -like "?*"){
				$tmp_msg_content = "<br/>"+$msg_content.caption.Replace("`n", "<br/>")
			}
			If (Test-Path -Path $tmp_filename){ # Teste ob Datei vorhanden
				If ($RealFilenames -And $tmp_real_filename -eq ""){
					$tmp_real_filename = GetFileSuffix $tmp_filename
				}
				Move-Item $tmp_filename $chat_id_folder/$tmp_filename$tmp_real_filename
				#Remove-Item $tmp_filename_thumbnail 2>$null # eigentlich kein Grund, das Thumbnail zu löschen. uncomment bei Bedarf
			} ElseIf (Test-Path -Path $tmp_filename_thumbnail){ # Teste ob Thumbnail vorhanden
				If ($RealFilenames){
					$tmp_real_filename = GetFileSuffix $tmp_filename_thumbnail
				}
				Move-Item $tmp_filename_thumbnail $chat_id_folder/$tmp_filename_thumbnail$tmp_real_filename
			}
			If (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename+$tmp_real_filename)){
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><a target=""blank"" href="""+$chat_id_folder+"/"+$tmp_filename+$tmp_real_filename+"""><img src="""+$chat_id_folder+"/"+$tmp_filename+$tmp_real_filename+"""></a><br/>"+$tmp_msg_content+"</p>"				
			} ElseIf (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename)){
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><a target=""blank"" href="""+$chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename+"""><img src="""+$chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename+"""></a><br/>"+$tmp_msg_content+"</p>"
			} Else { # leider weder Datei noch Thumbnail gefunden
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><i>Bild (Datei: "+$chat_id_folder+"/"+$tmp_filename+") nicht gefunden.</i><br/>"+$tmp_msg_content+"</p>"
			}
		break}
		#
		{"AUDIO","VOICEMESSAGE" -eq $_} {
			If (Test-Path -Path $tmp_filename){ # Teste ob Datei vorhanden
				If ($RealFilenames -And $tmp_real_filename -eq ""){ # nur wenn nix anderes da ist
					$tmp_real_filename = ".m4a"
				}
				Move-Item $tmp_filename $chat_id_folder/$tmp_filename$tmp_real_filename
			}
			If (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename+$tmp_real_filename)){
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><audio src="""+$chat_id_folder+"/"+$tmp_filename+$tmp_real_filename+""" controls><br/></p>"
			} Else { # Datei leider nicht gefunden
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><i>Audio (Datei: "+$chat_id_folder+"/"+$tmp_filename+") nicht gefunden.</i><br/></p>"
			}
		break}
		#
		"VIDEO" {
			If ($msg_content.caption -like "?*"){
				$tmp_msg_content = "<br/>"+$msg_content.caption.Replace("`n", "<br/>")
			}
			If (Test-Path -Path $tmp_filename){ # Teste ob Datei vorhanden
				If ($RealFilenames -And $tmp_real_filename -eq ""){ # nur wenn nix anderes da ist
					$tmp_real_filename = ".mp4"
				}
				Move-Item $tmp_filename $chat_id_folder/$tmp_filename$tmp_real_filename
				#Remove-Item $tmp_filename_thumbnail 2>$null # eigentlich kein Grund, das Thumbnail zu löschen. uncomment bei Bedarf
			} ElseIf (Test-Path -Path $tmp_filename_thumbnail){ # Teste ob Thumbnail vorhanden
				If ($RealFilenames){
					$tmp_real_filename = GetFileSuffix $tmp_filename_thumbnail
				}
				Move-Item $tmp_filename_thumbnail $chat_id_folder/$tmp_filename_thumbnail$tmp_real_filename
			}
			If (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename+$tmp_real_filename)){
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><video src="""+$chat_id_folder+"/"+$tmp_filename+$tmp_real_filename+""" controls><br/>"+$tmp_msg_content+"</p>"
			} ElseIf (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename)){
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><i>Video (Datei: "+$chat_id_folder+"/"+$tmp_filename+") nicht gefunden. Gespeichertes Vorschaubild:</i><br/><a target=""blank"" href="""+$chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename+"""><img src="""+$chat_id_folder+"/"+$tmp_filename_thumbnail+$tmp_real_filename+"""></a><br/>"+$tmp_msg_content+"</p>"
			} Else { # leider weder Video noch Thumbnail gefunden
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><i>Video (Datei: "+$chat_id_folder+"/"+$tmp_filename+") nicht gefunden.</i><br/>"+$tmp_msg_content+"</p>"
			}
		break}
		#
		"BALLOT" {
			$newballot = $msg_content.body.Replace("[","").Replace("]","").Split(",")
			#
			For ([long]$i=0; $i -lt $csv_content_ballot.Count; $i++){				
				If ($csv_content_ballot[$i].id -eq $newballot[1]){					
					# neue Umfrage
					If ($newballot[0] -eq "1"){
						$tmp_content += $tmp_content_from_user+"`t<p class=""ballot"">Umfrage """+$csv_content_ballot[$i].name+""" begonnen:</p>"
						For ([long]$j=0; $j -lt $csv_content_bchoice.Count; $j++){				
							If ($csv_content_bchoice[$j].ballot -eq $csv_content_ballot[$i].aid+"-"+$csv_content_ballot[$i].creator){
								$tmp_content += "`t<p class=""msg""><ballot-choice>"+$csv_content_bchoice[$j].name+"</ballot-choice></p>"
							}
						}
					# Umfrage geschlossen
					} ElseIf ($newballot[0] -eq "3"){
						$tmp_content += $tmp_content_from_user+"`t<p class=""ballot"">Umfrage """+$csv_content_ballot[$i].name+""" geschlossen:</p>"
						$ballotchoices = @()
						For ([long]$j=0; $j -lt $csv_content_bchoice.Count; $j++){				
							If ($csv_content_bchoice[$j].ballot -eq $csv_content_ballot[$i].aid+"-"+$csv_content_ballot[$i].creator){
								$ballotchoices += $csv_content_bchoice[$j].aid, $csv_content_bchoice[$j].name, ""
							}
						}
						#
						For ([long]$k=0; $k -lt $csv_content_bvote.Count; $k++){				
							If ($csv_content_bvote[$k].ballot_uid -eq $csv_content_ballot[$i].aid+"-"+$csv_content_ballot[$i].creator){
								If ($csv_content_bvote[$k].choice -eq 1){
									For ($l=0; $l -lt $ballotchoices.Count; $l+=3){
										If ($ballotchoices[$l] -eq $csv_content_bvote[$k].choice_uid){
											$tmp_username = GetContactName $csv_content_bvote[$k].identity $false $myOwnName
											If ($ShowThreemaIDs){
												$ballotchoices[$l+2] += ", <ballot-identity>"+$tmp_username+" ("+$csv_content_bvote[$k].identity+")</ballot-identity>"
											} Else {
												$ballotchoices[$l+2] += ", <ballot-identity>"+$tmp_username+"</ballot-identity>"
											}
										}
									}
								}
							}
						}
						For ($n=0; $n -lt $ballotchoices.Count; $n+=3){
							If ($ballotchoices[$n+2].Length -gt 2){ $ballotchoices[$n+2] = $ballotchoices[$n+2].Substring(2) }
						}
						For ($m=0; $m -lt $ballotchoices.Count; $m+=3){
							$tmp_content += "`t<p class=""msg""><ballot-choice>"+$ballotchoices[$m+1]+"</ballot-choice>: "+$ballotchoices[$m+2]+"</p>"
						}
					}
				}
			}
		break}
		#
		"VOIP_STATUS" {
			[string]$tmp_content_voice = ""
			[string]$tmp_content_voice_duration = ""
			[array]$tmp_voice_status = $msg_content.body.Split("{").Split("}")[1].Split(",").Replace("""", "") # Array of "Parameter:Value"
			For ([long]$i=0; $i -lt $tmp_voice_status.Count; $i++){
				[array]$tmp_voice_parameter = $tmp_voice_status[$i].Split(":") # in [0]->Parameter, [1]->Value
				If ($tmp_voice_parameter[0] -eq "status"){
					Switch ($tmp_voice_parameter[1]){
						"1" {
							$tmp_content_voice = "Verpasster Anruf"
						}
						"2" {
							If ($msg_content.isoutbox -eq 0){
								$tmp_content_voice = "Eingehender Anruf"
							} Else {
								$tmp_content_voice = "Ausgehender Anruf"
							}
						}
						"3" {
							$tmp_content_voice = "Anruf abgelehnt"
						}
						"4" {
							$tmp_content_voice = "Anruf abgebrochen"
						}
					}
				} ElseIf ($tmp_voice_parameter[0] -eq "duration"){
					$tmp_content_voice_duration = "<br/><span style=""font-size:0.8em;"">&ensp;&#9201; "+[timespan]::fromseconds($tmp_voice_parameter[1]).ToString("hh\:mm\:ss")+"</span>"
				}
			}
			# jetzt zusammenbasteln:
			$tmp_content = $tmp_content_from_user+"`t<table><tr><td><span class=""call-icon""></span></td><td><p class=""msg"">&ensp;"+$tmp_content_voice+$tmp_content_voice_duration+"</p></td></tr></table>"
		break}
		#
		"GROUP_CALL_STATUS" {
			If ($msg_content.body.Contains("status"":1")) { # Gruppenanruf gestartet
				[string]$tmp_content_caller_identity = $msg_content.body -match "callerIdentity"":""(\w+)" #CallerID steht nun (hoffentlich) in $Matches[1]
				$tmp_group_user_name = GetContactName $Matches[1] $false $myOwnName # Name holen
				If ($ShowThreemaIDs -And $Matches[1] -ne ""){
					$tmp_content_from_user = "`t<p class=""identity"">"+$tmp_group_user_name+"<ident-id> ("+$Matches[1]+")</ident-id></p>`n"
				} Else {
					$tmp_content_from_user = "`t<p class=""identity"">"+$tmp_group_user_name+"</p>`n"
				}
				$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><span class=""call-icon"">&ensp;Gruppenanruf gestartet</span></p>"
			} ElseIf ($msg_content.body.Contains("status"":2")) { # Gruppenanruf beendet (ist eine Status-Nachricht!)
				$msg_timestamp_readable = ConvertUnixTime $msg_content.created_at $true
				$tmp_content = "`t<p class=""msg""><span title="""+$msg_timestamp_readable+""">Gruppenanruf hat geendet</span></p>" # benutze Timestamp als MouseOver-Text
			}
		break}
		#
		"LOCATION" {
			[array]$tmp_location = $msg_content.body.Replace("[", "").Replace("]", "").Split(",")
			If ($msg_content.caption -like "?*"){
				$tmp_msg_content = $msg_content.caption.Replace("`n", "<br/>").Replace("*", "")
			} Else { # kein .caption? Hol die Daten aus dem .body
				$tmp_msg_content = $tmp_location[3].Replace("""", "")
			}
			$tmp_content = $tmp_content_from_user+"`t<p class=""msg""><b>Standort:</b><br/><a target=""blank"" href=""https://www.openstreetmap.org/search?query="+$tmp_location[0]+"%2C"+$tmp_location[1]+"#map=17/"+$tmp_location[0]+"/"+$tmp_location[1]+""">"+$tmp_msg_content+"</a></p>"
		break}
	}
	$tmp_content
}

# Funktion zum generieren des Zitats (neues System)!
function GetQuoteMessage([long]$message_index){
	[bool]$quote_not_found = $true
	[array]$content_quote = "`t<div class=""message quote"">"
	#
	If ($LinkQuotes){ # Zitat als Link definieren
		$content_quote += "`t`t<a href=""#id"+$csv_content_chat[$message_index].quoted_message_apiid+""" class=""quotelink"">"
	}
	For ([long]$o=$message_index; $o -ge 0; $o--){
		If ($csv_content_chat[$o].apiid -eq $csv_content_chat[$message_index].quoted_message_apiid){ # Zitat-Nachricht gefunden!
			$tmp_content_quote = WriteMessage $csv_content_chat[$o]
			$content_quote += "`t"+$tmp_content_quote
			$msg_timestamp_readable = ConvertUnixTime $csv_content_chat[$o].created_at $true
			$content_quote += "`t`t<p class=""timestamp"">"+$msg_timestamp_readable+"</p>"
			$quote_not_found = $false
			break
		}
	}
	If ($quote_not_found){  # ich habe heute leider kein Zitat für dich...
		$content_quote += "`t`t<p class=""msg""><i>Zitat (ID: "+$csv_content_chat[$message_index].quoted_message_apiid+") nicht gefunden.</i><br></p>"
		$content_quote += "`t`t<p class=""timestamp""><i>Zeitpunkt auch nicht.</i><br></p>"
	}
	If ($LinkQuotes){ # Link ordentlich schliessen
		$content_quote += "`t`t</a>"
	}	
	$content_quote += "`t</div>" # Container schliessen
	return $content_quote
}

# Funktion zum generieren des Nachrichten-Status (1:1) bzw. der Nachrichten-Stati (Gruppe)!
function GetMessageStatus($msg_content){
	[long]$msg_ack_count = 0
	[long]$msg_dec_count = 0
	[string]$tmp_msg_status = ""
	#
	If ($isGroup){
		If (-Not $msg_content.g_msg_states) { Return "" } # keine Stati vorhanden oder leer
	    $msg_stati = $msg_content.g_msg_states.Replace("{","").Replace("}","").Replace("""", "").Split(",")
		For ([long]$i=0; $i -lt $msg_stati.Count; $i++){
			# Name zusammenbasteln
			$tmp_user_name = GetContactName $msg_stati[$i].Split(":")[0] $false $myOwnName
			If ($ShowThreemaIDs){
				$tmp_user_name += " ("+$msg_stati[$i].Split(":")[0]+")"
			}
			Switch ($msg_stati[$i].Split(":")[1]){
				"USERACK" {
					If ($msg_ack_count -eq 0){ # erster Eintrag
						[string]$msg_ack = $tmp_user_name
					} Else {
						$msg_ack += ", "+$tmp_user_name
					}
					$msg_ack_count++
				}
				"USERDEC" {
					If ($msg_dec_count -eq 0){ # erster Eintrag
						[string]$msg_dec = $tmp_user_name
					} Else {
						$msg_dec += ", "+$tmp_user_name
					}
					$msg_dec_count++
				}
			}
		}
		If ($msg_ack_count -gt 0) {
			$tmp_msg_status = "<span style=""color:limegreen"" title="""+$msg_ack+""">&#128077;x"+$msg_ack_count+"</span>"+" "
		}
		If ($msg_dec_count -gt 0) {
			$tmp_msg_status += "<span style=""color:tomato"" title="""+$msg_dec+""">&#128078;x"+$msg_dec_count+"</span>"+" "
		}
	} Else {
		Switch ($msg_content.messagestae){
			"SENT" {$tmp_msg_status = "&#9993; "; break}
			"DELIVERED" {$tmp_msg_status = "&#128233; "; break}
			"READ" {$tmp_msg_status = "&#128065; "; break}
			"CONSUMED" {$tmp_msg_status = "&#128066; "; break}
			"USERACK" {$tmp_msg_status = "&#128077; "; break}
			"USERDEC" {$tmp_msg_status = "&#128078; "; break}
			"FAILED" {$tmp_msg_status = "&#10060; "; break}
			default {$tmp_msg_status = ""; break}
		}
	}
	return $tmp_msg_status
}

# Funktion zum Konvertieren des Chats
function ConvertChat([string]$chat_file) {
	[string]$chat_partner_name = ""
	[string]$chat_partner_ID = ""
	[string]$curDate = ""
	[string]$oldDate = ""
	[long]$message_count = 0
	#
	[array]$csv_content_chat = Import-Csv $chat_file
	# Teste auf leere Chat-CSV-Datei
	If ($csv_content_chat.Length -eq 0){
		Write-Host "Abbruch: Chat-Datei $chat_file hat keinen Inhalt!`n" -ForegroundColor Red
		Return # Exit 1
	}
	# Prüfe auf Gruppe oder Einzelchat/Kanal
	If (($csv_content_chat[0] | Get-Member -Name identity).Name -eq "identity"){
		[bool]$isGroup = $true
		[string]$chat_id = (Get-ChildItem $chat_file).BaseName.Split("_")[2].Split("-")[0]
	} Else {
		[bool]$isGroup = $false
		# Teste auf Kanal, ansonsten Einzelchat
		If ((Get-ChildItem $chat_file).BaseName.Split("_").Count -eq 3){
			[string]$chat_id = (Get-ChildItem $chat_file).BaseName.Split("_")[2]
		} Else {
			[string]$chat_id = (Get-ChildItem $chat_file).BaseName.Split("_")[1]
		}
	}
	# Wenn Kanal (mit * beginnend), dann setze den in der ID auch bitte
	If ($chat_id.Length -eq 7){ $chat_id = "*"+$chat_id }
	# Medienordner mit "gc_" oder "id_" beginnend
	If ($isGroup){
		[string]$chat_id_folder = "gc_"+$chat_id
		[string]$chat_id_identity = $chat_id
	} Else {
		[string]$chat_id_folder = "id_"+$chat_id.Replace("*", "_")
		[string]$chat_id_identity = $chat_id.Replace("*", "_")
	}
	# Lege neuen Ordner für Medien an
	New-Item -ItemType Directory $chat_id_folder >$null 2>$null
	#
	$chat_partner_name = GetContactName $chat_id $isGroup "nicht gefunden"
	#
	# Es geht los!
	If ($isGroup){
		Write-Host -nonewline "Konvertiere Gruppenchat ""$chat_partner_name"": "
	} Else {
		$chat_partner_ID = GetContactName $chat_id $false "&#128077;"  # so bekommen wir die Threema-ID!
		$SenderID = "" # im 1:1-Chat nicht möglich; deshalb sicherheitshalber leer machen
		Write-Host -nonewline "Konvertiere Chat mit $chat_partner_name ($chat_id)",@(("("+$chat_partner_ID+")"),"")[$chat_partner_ID -eq ""],": " -Separator ""
	}
	If ($ShowAllIdentities -eq $false -Or $myOwnName -eq "") {$myOwnName = "Ich"} # wichtig, falls übergebener Name nicht gewünscht wird, oder leer ist aber doch irgendwo benötigt wird
	If (-Not ($csv_content_chat[0] | Get-Member -Name quoted_message_apiid).Name -eq "quoted_message_apiid") {$LinkQuotes = $false} # keine ID? dann gar nicht erst versuchen
	If ($CSS) {$WebStyle = $true} # damit bei externer CSS auch der erweiterte Header geschrieben wird
	#
	# Style und Farben auswählen
	[bool]$tmp_DarkMode = $DarkMode # nötig, um [switch] zu [bool] zu konvertieren...
	If ($WebStyle){
		[string]$color_window_bg = @("#d3d3d3","grey")[$tmp_DarkMode]
		[string]$color_header_bg = @("#f6f6f6","dimgrey")[$tmp_DarkMode]
		[string]$color_header_text = @("inherit","whitesmoke")[$tmp_DarkMode]
		[string]$color_chat_bg = @("#efebe9","black")[$tmp_DarkMode]
		[string]$color_scrollbar = @("#ccc #f6f6f6","whitesmoke dimgrey")[$tmp_DarkMode]
		[string]$color_general_bg = @("#fcf8e3","#086A87")[$tmp_DarkMode]
		[string]$color_sent_bg = @("#dff0d8","#585858")[$tmp_DarkMode]
		[string]$color_sent_time_text = @("#999","darkgrey")[$tmp_DarkMode]
		[string]$color_received_bg = @("#fff","#6E6E6E")[$tmp_DarkMode]
		[string]$color_received_time_text = @("#aaa","darkgrey")[$tmp_DarkMode]
		[string]$color_quote_border = @("#181818","darkorange")[$tmp_DarkMode]
		[string]$color_body_text = @("rgba(0, 0, 0, 0.87)","rgba(255, 255, 255, 0.87)")[$tmp_DarkMode]
		[string]$color_identity_text = @("#181818","#DBA901")[$tmp_DarkMode]
		[string]$color_ballot_identity_text = @("inherit","#DBA901")[$tmp_DarkMode]
		[string]$color_mark_bg = @("#e0e0e0","darkgrey")[$tmp_DarkMode]
		[string]$color_ballot_text = @("grey","#81BEF7")[$tmp_DarkMode]
	} Else {
		[string]$color_body_text = @("black","white")[$tmp_DarkMode]
		[string]$color_body_bg = @("lightgrey","black")[$tmp_DarkMode]
		[string]$color_date_bg = @("lightblue","#424242")[$tmp_DarkMode]
		[string]$color_general_text = @("inherit","#81BEF7")[$tmp_DarkMode]
		[string]$color_general_bg = @("lightyellow","#086A87")[$tmp_DarkMode]
		[string]$color_send_bg = @("#EAFAF1","#585858")[$tmp_DarkMode]
		[string]$color_received_bg = @("white","#6E6E6E")[$tmp_DarkMode]
		[string]$color_quote_bg = @("lightyellow","#424242")[$tmp_DarkMode]
		[string]$color_name_text = @("inherit","#DBA901")[$tmp_DarkMode]
		[string]$color_id_text = @("inherit","#DBA901")[$tmp_DarkMode]
		[string]$color_time_text = @("grey","#BDBDBD")[$tmp_DarkMode]
		[string]$color_ballot_text = @("grey","#81BEF7")[$tmp_DarkMode]
	}
	#
	Write-Host -nonewline "$($csv_content_chat.Count) Datensätze gesamt..."
	#
	# HTML Anfang
	[array]$content = "<!-- Generated on " + (Get-Date -Format "yyyy-MM-dd HH:mm") + " by threema-csv2html (Version: $my_version) -->"
	$content += "<!DOCTYPE html>"
	$content += "<html>"
	$content += "<head>"
	# Setze des Seitentitels und Gruppen-/Chat-/Kanal-Foto identifizieren und kopieren
	[string]$tmp_filename_suffix = "" # Init
	If ($isGroup){
		$content += "`t<title>Threema Gruppenchat: "+$chat_partner_name+"</title>"
		[string]$tmp_filename = "group_avatar_"+(Get-ChildItem $chat_file).BaseName.Split("_")[2]
		If ($RealFilenames){
			$tmp_filename_suffix = GetFileSuffix $tmp_filename
		}
		Copy-Item $tmp_filename $chat_id_folder/$tmp_filename$tmp_filename_suffix 2>$null
	} Else {
		If ($chat_id_identity.StartsWith("_")){
			$content += "`t<title>Threema Kanal: "+$chat_partner_name+" ("+$chat_id+")</title>"
		} Else {
			$content += "`t<title>Threema Chat: "+$chat_partner_name+" ("+$chat_id+")</title>"
		}
		[string]$tmp_filename = "contact_profile_pic_"+(Get-ChildItem $chat_file).BaseName.Split("_")[1] # 1. Versuch mit profile
		If ($RealFilenames){
			$tmp_filename_suffix = GetFileSuffix $tmp_filename
		}
		Copy-Item $tmp_filename $chat_id_folder/$tmp_filename$tmp_filename_suffix 2>$null
		If (-Not (Test-Path -Path ($chat_id_folder+"/"+$tmp_filename+$tmp_filename_suffix))){ # 2. Versuch mit avatar
			[string]$tmp_filename = "contact_avatar_"+(Get-ChildItem $chat_file).BaseName.Split("_")[1]
			If ($RealFilenames){
				$tmp_filename_suffix = GetFileSuffix $tmp_filename
			}
			Copy-Item $tmp_filename $chat_id_folder/$tmp_filename$tmp_filename_suffix 2>$null
		}
	}
	# Setze das Chatfoto
	$content += "`t<link href="""+$chat_id_folder+"/"+$tmp_filename+$tmp_filename_suffix+""" rel=""icon"" type=""image/x-icon""/>"
	If ($CSS){ # Verlinke das Layout
		$content += "`t<link rel=""stylesheet"" href=""$CSS"">"
	} Else { # Schreibe das Layout
		[string]$CallIcon = "content: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAUrSURBVFhHvZdfTFNXHMe/997S0paW/38sIjAQKDpUsIooiMOZbEiyOcnC4ssS4x7mHnyZT4sPy/a0ZFnQF83i5pK5ZdG5mC17MEbMgv+qTCuEqAEcLf6Baktb2tL2nv3O7Z1jCm0B2Sc5wD2/wz3fe8739zv3CniO3QcO6H0ul3U8EG6fZoKNAcshCAI1dUSKMPpPxpgE3DeI6C3KMZ8+c+LEEI/EB8SZeVehvaur0ukLfhBlQgcTWLnABI0y+SJhshyjm9zVi/ih3lJw5OjRoxNqCCQQOHTokGhYtmyb0xf+nEZ2QhAL6ZGllzE5h24j0o+8CGObH/unrJs3bLYP3Op7wmOKAJPFUu3yhT6LQmijsVretxSQECnGWNVkOFj8Wtu2cw67PSTt27fPMOj2HIwI4js0IE0du3TQJDGZVfp9gYm9Xe9elcxllTZfVD7Il4iH46OWGFrmiMzKYlORs6I3FOqQgVLeHY8uHI0kIU2jUa8SQv5GhXPy6U4pv6rmIBPElWpgwRTkZKOjtQltGxsUEaOPxnkWqtEXIXuLMTnmF6MyStS+WUnXaVFdtgJ1VRVKKyteBvG55ODJsqW+DlOhMH69eAnNDWtQUWJRo3NBXoCwXMqvrv2IbsD3f1ZqK8rw8fvvYXujDa22dbCttqJv8A4m/QF1BGDJz8UbzY2IxmLQ0tMX5uUgKyMD/feGISdYBRLhFNW/5oQ/ncloRO+fDtj7B5GTaUJuplmNxtnw6io01FZj9+ut+LBrF7asq0Nb43oU5GarI+aAFjKpAF9gCtORCNyeSVy41gdJFJFP+z0TbZoGEhmQwwXzxg2pS0ue1UkFuD1ePJ30oXJFMQz6dFAOY3lhvhpdPMlXYCqIIecYVq98BXvad5AgD27T3s4kGA5Djskk7t8WiUaV38mgNLTuT2RCnko8rfi++oNBHD/zG+y3B/+TYo8mnsA1Pk7mvKv4hLcrtwZw975TMWYCRoXa9rcHSUC12jErmaYMfLp/LwmR8MnhrzHx1KNGFgcD6026BRyvz4+zF/5AUV4udm5tema4l0FKAjgXr9/EpZu30dHShO1U7bjTZ/L8daok9cA/xMhk9/5ykhkrsHX9Wnj9ftx/8AgyZQVPw+b6NWhv2YTC3ByqFWbFN94ZxWoORlMWwAkEQxgYGsGqynJs32RDtslE5pvApjWrsWfnDpQXW6gg1SileG3NSsWMfsqiBMxPAIeXYF6K87KzlNK8sY5XwSpMkbjDJ0/j/NUbMGcYqVYUKNvmIf8kYP4COHwleCo+GHfT4VSkmFNLVY+XXkN6OjJJABd4/sr1pRHA4UVm2PUAl2/2K/keiUVJSA5qyktRRD5weyfRY+9TSvmcMF4H3nxrUBDFhHUgFXgSGPV65en1Op1SgJyPxxGNzlmIGJNjvaIkYIxfxPsWDi+M3HCuxxO4N+rCyNjDRJMr0NzDolZglxO+uiwR9K0Q0oHZRXOa7keB4aHa/3/BaMsGLFmm38WWOmu/RmSn+NeLGlx6mBwwiOLJIqNxSOrp6ZHbWrY43P7gRjo8SxZcU1OEQY5oBeGUtSj3q2+OHfMop4rjxg2fbUODIxCarqQvlxWkIeUzYh7Q4ScHdCJ+LjVnfvHTt8fv8M5nx9odh2OstbnpHDl5OsJoJRgzv6TV4BOH6Ub9Jo14pMRk+PLM998pk3NemKCzs1MK6fU1Lrd3V4QJjXTWWOg9PJ7o84EnlizL9Ao5YhCEayaj6RdrQdZQd3d3WB1BAH8DjS0MO7pWbHMAAAAASUVORK5CYII=);"
		$content += "<style>"
		If ($WebStyle){
			$content += "* {`n`tpadding: 0;`n`tmargin: 0;`n`tbox-sizing: border-box;`n}`nbody {`n`theight: 100vh;`n`tbackground-color: $color_window_bg;`n`tfont-family: ""Roboto"", ""Helvetica Neue"", Helvetica, Arial, sans-serif;`n`tfont-size: 15px;`n`tbackground-size: cover;`n`tdisplay: flex;`n`tflex-direction: column;`n`talign-items: center;`n`tjustify-content: center;`n}`n.main {`n`tdisplay: flex;`n`tflex-direction: column;`n`theight: 95%;`n`tmax-width: 1000px;`n`tmin-width: 440px;`n}`n.main .header {`n`tz-index: 1;`n`tflex: 0 0 68px;`n`tdisplay: flex;`n`tflex-direction: row;`n`talign-items: center;`n`tpadding: 0 8px;`n`tbackground-color: $color_header_bg;`n`tborder-top-left-radius: 5px;`n`tborder-top-right-radius: 5px;`n`tbox-shadow: 1px 2px 4px -2px rgba(0, 0, 0, 0.2);`n}`n.main .header .avatar {`n`tposition: relative;`n`tmargin-left: 8px;`n}`n.main .header .avatar a {`n`ttext-decoration: none;`n`tcolor: inherit;`n`tdisplay: flex;`n}`n.main .header .avatar object {`n`tborder-radius: 50%;`n`tbackground-color: #fff;`n`twidth: 48px;`n`theight: 48px;`n`tpointer-events: none;`n}`n.main .header .avatar object::after {`n`tcontent: attr(alt);`n`tdisplay: flex;`n`tbackground-color: bisque;`n`tborder-radius: 50%;`n`tjustify-content: center;`n`tline-height: 48px;`n`tfont-weight: bold;`n}`n.main .header .details {`n`tmargin-left: 8px;`n`tcolor: $color_header_text;`n}`n.main .header .details .name {`n`tfont-weight: bold;`n`tdisplay: inherit;`n`tmax-width: 100%;`n`toverflow: hidden;`n`ttext-overflow: ellipsis;`n`tline-height: 1.3;`n`twhite-space: nowrap;`n`tmargin-bottom: 4px;`n}`n.main .header .details .id {`n`tfont-size: 0.9em;`n}`n.main .container {`n`tflex: 1 0 calc(95% - 68px);`n`twidth: 100%;`n`tpadding: 8px;`n`tmargin: 0 auto;`n`tbackground-color: $color_chat_bg;`n`tbox-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 5px 0 rgba(0, 0, 0, 0.23);`n`tborder-bottom-left-radius: 5px;`n`tborder-bottom-right-radius: 5px;`n`tposition: relative;`n`tscrollbar-width: thin;`n`tscrollbar-color: $color_scrollbar;`n`toverflow: auto;`n`toverflow-y: scroll;`n}"
			$content += ".main .container .date {`n`tdisplay: flex;`n`tflex-direction: column;`n}`n.main .container .date h2 {`n`tfont-size: 1em;`n`tfont-weight: normal;`n`ttext-align: center;`n`tcolor: #fff;`n`tmargin: 8px auto 16px auto;`n`tpadding: 8px;`n`tbackground-color: #424242;`n`tbox-shadow: -2px 2px 4px -2px rgba(0, 0, 0, 0.8);`n`tborder-radius: 5px;`n}`n.main .container .date .message {`n`tpadding: 8px;`n`tmin-width: 64px;`n`tmax-width: 85%;`n`tbox-shadow: 0 1px 1px rgba(0, 0, 0, 0.25);`n`tborder-radius: 5px;`n`tmargin-bottom: 8px;`n}`n.main .container .date .message.general {`n`tbackground-color: $color_general_bg;`n`talign-self: center;`n`ttext-align: center;`n}`n.main .container .date .message.sent {`n`tbackground-color: $color_sent_bg;`n`talign-self: end;`n`ttext-align: left;`n}`n.main .container .date .message.sent .timestamp {`n`ttext-align: right;`n`tcolor: $color_sent_time_text;`n}`n.main .container .date .message.received {`n`tbackground-color: $color_received_bg;`n`talign-self: start;`n`ttext-align: left;`n}`n.main .container .date .message.received .timestamp {`n`ttext-align: left;`n`tcolor: $color_received_time_text;`n}`n.main .container .date .message .quote {`n`tborder-left: 5px solid $color_quote_border;`n`tpadding-left: 5px;`n`tfont-size: 0.9em;`n`tmargin-bottom: 8px;`n`tmax-width: 95%;`n`tbackdrop-filter: brightness(0.95);`n}`n.main .container .date .message .quote .quotelink {`n`ttext-decoration: none;`n`tcolor: inherit;`n}`n.main .container .date .message .quote .timestamp {`n`tdisplay: none;`n}`n.main .container .date .message .msg {`n`tfont-size: 1em;`n`tline-height: 1.3em;`n`tmargin: 0;`n`tcolor: $color_body_text;`n`twhite-space: pre-wrap;`n`toverflow-wrap: anywhere;`n}`n.main .container .date .message .identity {`n`tfont-size: 0.9em;`n`tfont-weight: bold;`n`tcolor: $color_identity_text;`n`tmargin-bottom: 8px;`n`tdisplay: inline-block;`n}`n.main .container .date .message .identity > ident-id {`n`t`tfont-weight: normal;`n}`n.main .container .date .message .msg > ballot-choice {`n`tfont-weight: bold;`n}"
			$content += ".main .container .date .message .msg > ballot-identity {`n`tcolor: $color_ballot_identity_text;`n}`n.main .container .date .message .msg mark {`n`tborder-radius: 5px;`n`tpadding: 2px 7px;`n`tline-height: 10pt !important;`n`tbackground-color: $color_mark_bg;`n`tfont-weight: lighter;`n`tcolor: inherit;`n`tbox-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);`n}`n.main .container .date .message .msg a {`n`tcolor: #05a63f;`n`ttext-decoration: none;`n`ttransition: color 0.4s;`n}`n.main .container .date .message .msg a:hover {`n`tcolor: #036325;`n}`n.main .container .date .message .msg img {`n`twidth: 360px;`n}`n.main .container .date .message .msg audio {`n`tborder-radius: 5px;`n}`n.main .container .date .message .msg video {`n`twidth: 360px;`n}`n.main .container .date .message .msg .file {`n`tfont-size: normal;`n}"
			$content += ".main .container .date .message .call-icon::before {`n`t$CallIcon`n`tvertical-align: middle;`n`tdisplay: inline-block;`n}"
			$content += ".main .container .date .message .ballot {`n`tcolor: $color_ballot_text;`n`tfont-style: italic;`n`tmargin-top: 8px;`n`tmargin-bottom: 8px;`n}`n.main .container .date .message .timestamp {`n`tfont-size: 0.8em;`n`tmargin-top: 4px;`n}`n@media (max-width: 1000px) {`n`t`tbody {`n`t`t`t`tbackground: none;`n`t`t}`n`t.container {`n`t`theight: 100%;`n`t`tborder-radius: 0;`n`t}`n}"
		} Else {
			$content += "body {`n`tbackground-color: $color_body_bg;`n`tcolor: $color_body_text;`n`tfont-family: helvetica, sans-serif;`n`tfont-size: medium;`n`tpadding: 1ex;`n}`ndiv.date {`n`tpadding: 1ex;`n`tmargin: 1ex;`n}`ndiv.date h2 {`n`tbackground-color: $color_date_bg;`n`twidth: 120px;`n`tpadding: 1ex;`n`tborder-radius: 15px;`n`tmargin: auto;`n`tmargin-bottom: 1ex;`n`ttext-align: center;`n}`ndiv.general {`n`tbackground-color: $color_general_bg;`n`tcolor: $color_general_text;`n`twidth: 45ch;`n`tmargin: auto;`n`tborder-radius: 25px;`n`ttext-align: center;`n}`ndiv.general p {`n`tmargin: 5px;`n`tpadding: 5px;`n}`ndiv.sent {`n`tbackground-color: $color_send_bg;`n`tpadding: 1ex;`n`tmargin: 1ex;`n`tmin-width: 80%;`n`tmargin-left: 20%;`n`tborder-right: 1px solid grey;`n`tborder-bottom: 1px solid grey;`n`tborder-radius: 20px 0px 20px 20px;`n`ttext-align: left;`n}`ndiv.received {`n`tbackground-color: $color_received_bg;`n`tpadding: 1ex;`n`tmargin: 1ex;`n`twidth: 80%;`n`tborder-right: 1px solid grey;`n`tborder-bottom: 1px solid grey;`n`tborder-radius: 0px 20px 20px 20px;`n`ttext-align: left;`n}`ndiv.quote {`n`tbackground-color: $color_quote_bg;`n`tpadding: 1ex;`n`tmargin: 1ex;`n`tborder-right: 2px solid grey;`n`tborder-bottom: 2px solid grey;`n`tborder-radius: 20px 20px 20px 20px;`n`ttext-align: left;`n}`np {`n`tmargin: 0;`n}`np.identity {`n`tfont-weight: bold;`n`tcolor: $color_name_text;`n}`np.identity > ident-id {`n`tfont-weight: normal;`n`tcolor: $color_id_text;`n}`np.timestamp {`n`tmargin: 1ch;`n`tcolor: $color_time_text;`n`tfont-size: small;`n}`np.file {`n`tfont-size: medium;`n}`np.ballot {`n`tcolor: $color_ballot_text;`n`tfont-style: italic;`n`tmargin-top: 1em;`n}`nballot-choice {`n`tfont-weight: bold;`n}`nballot-identity {`n`tcolor: $color_name_text;`n}`nimg {`n`tborder-radius: 15px;`n`twidth: 360px;`n}`nvideo {`n`twidth: 360px;`n}`na.quotelink {`n`ttext-decoration: none;`n`tcolor: inherit;`n}"
			$content += "span.call-icon::before {`n`t$CallIcon`n`tvertical-align: middle;`n`tdisplay: inline-block;`n}"
		}
		$content += "</style>"
	}
	$content += "</head>"
	$content += "<body>"
	If ($WebStyle){
		$content += "<div class=""main"">"
		# User/Group Header!!!
		$content += "<div class=""header"">"
		$content += "`t<div class=""avatar"">"
		$content += "`t`t<a href="""+$chat_id_folder+"/"+$tmp_filename+$tmp_filename_suffix+""" target=""_blank"">"
		$content += "`t`t`t<object data="""+$chat_id_folder+"/"+$tmp_filename+$tmp_filename_suffix+""" type=""image/jpg"" alt=""&#10060;""></object>"
		$content += "`t`t</a>" # a href
		$content += "`t</div>" # avatar
		$content += "`t<div class=""details"">"
		$content += "`t`t<div class=""name"">"+$chat_partner_name+"</div>"
		$content += "`t`t<div class=""id"">"+$chat_partner_ID+"</div>"
		$content += "`t</div>" # details
		$content += "</div>" # header
		$content += "<div class=""container"">"
	}
	# Init für Dateinamen
	$chat_partner_name = $chat_partner_name -Replace "[^\u0000-\u0FFF]" -Replace '[\\\/\:\"\*\?\<\>\|]' # erzeuge gültigen Dateinamen
	[string]$nowDate = Get-Date -Format "yyyy-MM-dd.HHmm" # aktuelles Datum und Uhrzeit für Dateinamen
	If ($isGroup){
		$out_filename = ".\Chatgruppe $chat_partner_name $nowDate.html"
	} Else {
		$out_filename = ".\Chat mit $chat_partner_name ($chat_id_identity) $nowDate.html"
	}
	Remove-Item $out_filename 2>$null # löschen falls vorhanden, da Schreiboperationen nun "Append" benutzen
	#
	# Init für die Progress-Bar
	[double]$progress_steps = $csv_content_chat.Count / 100
	[long]$progress_steps_counter = 0
	#
	# Verarbeite jede einzelne Nachricht aus der Backup CSV Datei
	For ([long]$i=0; $i -lt $csv_content_chat.Count; $i++){
		# Init
		[bool]$msg_is_status = $false
		[bool]$msg_is_quote = $false
		#
		###################### Progress-Bar #########################
		If ($i -ge (($progress_steps * $progress_steps_counter)-1)) {
			Write-Progress -Activity "Ich verwurschtel gerade" -Status ("Datensatz "+($i+1)) -PercentComplete ($progress_steps_counter++)
			#
			$content | Out-File -LiteralPath $out_filename -Encoding UTF8 -Append # den Teil schon mal speichern
			[array]$content = $null
			#
			If ($progress_steps_counter -gt 100) { Start-Sleep -Seconds 1 } # zum Angucken
		}
		#############################################################
		#
		# Zeiteingrenzung
		If ([long]$csv_content_chat[$i].created_at -lt $num_FirstDate){
			continue # zu früh
		} ElseIf ([long]$csv_content_chat[$i].created_at -gt $num_LastDate){
			break # hier kommt nix mehr
		}
		#
		# Prüfe Datum und füge Tagesbanner ein
		$curDate = ConvertUnixTime $csv_content_chat[$i].created_at $false
		If ($curDate -ne $oldDate){
			If ($oldDate -ne ""){
				$content += "</div>"
			}
			$content += "<div class=""date""><h2>"+$curDate+"</h2>"
			$oldDate = $curDate
		}
		#
		$msg_status = GetMessageStatus($csv_content_chat[$i])
		#
		# Status-Nachricht
		If ($csv_content_chat[$i].isstatusmessage -eq 1){
			$msg_is_status = $true
			$content += "<div class=""message general"">"
			$content += WriteMessage $csv_content_chat[$i]
		} Else {
			# empfangene Nachricht
			If ($csv_content_chat[$i].isoutbox -eq 0){
				#
				If ($SenderID -ne "" -And $csv_content_chat[$i].identity -eq $SenderID){ # war empfangen, wird jetzt gesendet!
					$content += "<div class=""message sent"""+@(">",(" id=""id"+$csv_content_chat[$i].apiid+""">"))[$LinkQuotes -And $csv_content_chat[$i].apiid -ne ""]
				} Else {
					$content += "<div class=""message received"""+@(">",(" id=""id"+$csv_content_chat[$i].apiid+""">"))[$LinkQuotes -And $csv_content_chat[$i].apiid -ne ""]
				}
				#
				If ($csv_content_chat[$i].quoted_message_apiid){  # Quote-ID vorhanden...also Zitat erstellen! ("" oder $Null -> $false)
					$msg_is_quote = $true
					$content += GetQuoteMessage($i)
					$msg_is_quote = $false
				}
				$content += WriteMessage $csv_content_chat[$i]
				#
				$msg_timestamp_readable = ConvertUnixTime $csv_content_chat[$i].created_at $true
				$msg_timestamp_readable = $msg_status+$msg_timestamp_readable
				If ($csv_content_chat[$i].modified_at -ne ""){ # oh, eine geänderte Nachricht
					$modtime = ConvertUnixTime $csv_content_chat[$i].modified_at $true
					#$msg_timestamp_readable = $msg_timestamp_readable+"  (zuletzt geändert: "+$modtime+")" # zu viele Zahlen! uncomment für Anzeige
				}
			# gesendete Nachricht
			} Else {
				#
				If ($SenderID -ne ""){ # war gesendet, wird jetzt empfangen!
					$content += "<div class=""message received"""+@(">",(" id=""id"+$csv_content_chat[$i].apiid+""">"))[$LinkQuotes -And $csv_content_chat[$i].apiid -ne ""]
				} Else {
					$content += "<div class=""message sent"""+@(">",(" id=""id"+$csv_content_chat[$i].apiid+""">"))[$LinkQuotes -And $csv_content_chat[$i].apiid -ne ""]
				}
				#
				If ($csv_content_chat[$i].quoted_message_apiid){  # Quote-ID vorhanden...also Zitat erstellen! ("" oder $Null -> $false)
					$msg_is_quote = $true
					$content += GetQuoteMessage($i)
					$msg_is_quote = $false
				}
				$content += WriteMessage $csv_content_chat[$i]
				#
				$msg_timestamp_readable = ConvertUnixTime $csv_content_chat[$i].created_at $true
				If ($csv_content_chat[$i].modified_at -ne ""){ # oh, eine geänderte Nachricht
					$modtime = ConvertUnixTime $csv_content_chat[$i].modified_at $true
					$msg_timestamp_readable = $msg_timestamp_readable+"  ( "+$msg_status+" "+$modtime+")"
				}
			}
			#Timestamp für die Nachricht anhängen
			$content += "`t<p class=""timestamp"">"+$msg_timestamp_readable+"</p>"
		}
		$content += "</div>"
		$message_count++ # mitzählen!
	}
	# HTML Ende
	$content += "</div>" # class "date"
	If ($WebStyle){ # erweiterte Container wieder schliessen
		$content += "</div>" # class "container"
		$content += "</div>" # class "main"
	}	
	$content += "</body>"
	$content += "</html>"
	#
	$content | Out-File -LiteralPath $out_filename -Encoding UTF8 -Append # den letzten Rest speichern
	#
	Copy-Item $chat_file $chat_id_folder/ 2>$null # die *.csv-Datei mit in den Medienordner kopieren
	#
	Write-Host "$message_count konvertiert."
	Write-Host "Medienordner: .\$chat_id_folder`tHTML-Datei: $out_filename`n"
}

############################# MAIN #############################
# Informationen über mich
Write-Host # NewLine
Write-Host "Threema csv2html Konvertierungstool Version $my_version"

If ($Command_or_ChatFile -eq "help") {
	Write-Output $HelpText
	exit
}
Write-Host # NewLine
# notwendige Dateien initialisieren
If ($Command_or_ChatFile -notmatch "listc(ontacts)?$") {
	# groups.csv
	Try {
		[array]$csv_content_groups = Import-Csv "groups.csv"
	} Catch {
		Write-Host "Abbruch: Datei ""groups.csv"" nicht gefunden!" -ForegroundColor Red
		Write-Host # NewLine
		Exit 2
	}
}
If ($Command_or_ChatFile -notmatch "listg(roups)?$") {
	# contacs.csv
	Try {
		[array]$csv_content_contacts = Import-Csv "contacts.csv"
	} Catch {
		Write-Host "Abbruch: Datei ""contacts.csv"" nicht gefunden!" -ForegroundColor Red
		Write-Host # NewLine
		Exit 2
	}
}
If ($Command_or_ChatFile -match "listg(roups)?$") {
	# list Groups
	If (($csv_content_groups[0] | Get-Member -Name group_uid).Name -eq "group_uid"){ # aktuelles Datenbankformat
		$csv_content_groups |  ft -Property `
			@{Label=" Creator  "; expr={" "+$_.creator}},
			@{Label="Group Name"; expr={$_.groupname}},
			@{Label="Identity  "; expr={$_.group_uid+" "}},
			@{Label="Chatfile"; expr={"group_message_"+$_.group_uid+".csv"}}
	} Else { # älteres Datenbankformat
		$csv_content_groups | ft -Property `
			@{Label=" Creator  "; expr={" "+$_.creator}},
			@{Label="Group Name"; expr={$_.groupname}},
			@{Label="Chatfile"; expr={"group_message_"+$_.id+"-"+$_.creator+".csv"}}
	}
	Write-Host "Anzahl der Gruppen:"$csv_content_groups.Count"`n"
} ElseIf ($Command_or_ChatFile -match "listc(ontacts)?$") {
	# list Contacts
	If (($csv_content_contacts[0] | Get-Member -Name identity_id).Name -eq "identity_id"){ # aktuelles Datenbankformat
		$csv_content_contacts | ft -Property `
			@{Label="Threema-ID"; expr={" "+$_.identity}},
			@{Label="Name"; expr={$_.firstname+" "+$_.lastname}},
			@{Label="Nickname"; expr={$_.nick_name+" "}},
			@{Label="Identity  "; expr={$_.identity_id+" "}},
			@{Label="Chatfile"; expr={"message_"+$_.identity_id+".csv"}}
	} Else { # älteres Datenbankformat
		$csv_content_contacts | ft -Property `
			@{Label="Threema-ID"; expr={" "+$_.identity}},
			@{Label="Name"; expr={$_.firstname+" "+$_.lastname}},
			@{Label="Nickname"; expr={$_.nick_name+" "}},
			@{Label="Chatfile"; expr={"message_"+$_.identity+".csv"}}
	}
	Write-Host "Anzahl der Kontakte:"$csv_content_contacts.Count"`n"
} Else { # convert Chat(s)
	# Init (hierher verschoben)
	[long]$num_FirstDate = 128390400000 # 26.01.1974 * 1000
	[long]$num_LastDate = 4102358400000 # 31.12.2099 * 1000
	#
	Write-Host -nonewline @(("Eigener Name: """+$myOwnName+"""`n"),"")[$myOwnName -eq ""]
	Write-Host -nonewline @(("Eigene Threema-ID: """+$myOwnThreemaID+"""`n"),"")[$myOwnThreemaID -eq ""]
	Write-Host -nonewline @("","Threema-IDs verwenden`n")[[bool]$ShowThreemaIDs]
	Write-Host -nonewline @("","Immer Absender verwenden`n")[[bool]$ShowAllIdentities]
	Write-Host -nonewline @("","Klickbare Zitate erstellen`n")[[bool]$LinkQuotes]
	Write-Host -nonewline @("","Echte Dateinamen verwenden`n")[[bool]$RealFilenames]
	If ($CSS -eq ""){
		Write-Host -nonewline @("","Threema Web Design verwenden`n")[[bool]$WebStyle]
		Write-Host -nonewline @("","Dunkles Design verwenden`n")[[bool]$DarkMode]
	} Else {
		$CSS = $CSS.Replace("\","/")
		Write-Host "Verwende externe CSS-Datei: ""$CSS"""
	}
	#
	If ($FirstDate -or $LastDate) {
		Write-Host -nonewline "Zeitraum: "
		If ($FirstDate){
			Try {
				$FirstDate = Get-Date -Date $FirstDate -format (Get-Culture).DateTimeFormat.ShortDatePattern
				$num_FirstDate = ((Get-Date -Date $FirstDate -uformat %s) + "000") # Sekunden * 1000
			} Catch {
				Write-Host "`n`nAbbruch: <FirstDate> hat kein gültiges Datumsformat!" -ForegroundColor Red
				Write-Host # NewLine
				Exit 4
			}
			Write-Host -nonewline "von $FirstDate bis"
		} Else {
			Write-Host -nonewline "von Anfang bis"
		}
		If ($LastDate){
			Try {
				$LastDate = Get-Date -Date $LastDate -format (Get-Culture).DateTimeFormat.ShortDatePattern
				$num_LastDate = ((Get-Date -Date $LastDate -uformat %s) + "000") # Sekunden * 1000
				$num_LastDate = $num_LastDate + 86399999 # bis Ende dieses Tages, also +(60*60*24)*1000-1
			} Catch {
				Write-Host "`n`nAbbruch: <LastDate> hat kein gültiges Datumsformat!" -ForegroundColor Red
				Write-Host # NewLine
				Exit 4
			}
			Write-Host " $LastDate"
		} Else {
			Write-Host " Ende"
		}
	}
	Write-Host # NewLine
	# Teste auf Vorhandensein der Umfrage-Dateien
	Try {
		[array]$csv_content_ballot = Import-Csv "ballot.csv"
		[array]$csv_content_bvote = Import-Csv "ballot_vote.csv"
		[array]$csv_content_bchoice = Import-Csv "ballot_choice.csv"
	} Catch {
		Write-Host "Abbruch: Nicht alle ""ballot*""-Dateien (für Umfragen) gefunden!" -ForegroundColor Red
		Write-Host # NewLine
		Exit 3
	}
	# Init Ende
	###########
	[array]$csv_file_list = Get-ChildItem -filter $Command_or_ChatFile -name -exclude ballot*,contacts.csv,distribution_list.csv,groups.csv,nonces.csv
	If ($csv_file_list.Count -eq 0){
		Write-Host "Abbruch: keine Chat-Datei(en) namens $Command_or_ChatFile gefunden!" -ForegroundColor Red
		Write-Host # NewLine
		Exit 1
	}
	For ([long]$i=0; $i -lt $csv_file_list.Count; $i++){
		Write-Host -nonewline "($($i+1)/$($csv_file_list.Count)) " # (x/y)
		ConvertChat $csv_file_list[$i]
	}
	Write-Host "Fertig!"
	Write-Host # NewLine
}
exit
