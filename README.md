# Threema Chat Export

Das hier verfügbare Script bietet die Möglichkeit, aus Konversationen eines Threema Android Daten-Backups ordentlich lesbare HTML-Versionen zu erstellen.  
Das Script wird als Microsoft PowerShell-Script zur Verfügung gestellt und kann unter Windows, Linux und macOS ausgeführt werden.

Fragen, Diskussionen, Danksagungen und Kommentare bitte im <a href="https://threema-forum.de/index.php?thread/5505-chat-aus-daten-backup-in-html-konvertieren/" target="_blank">entsprechenden Thread bei threema-forum.de</a>. Vielen Dank!

### aktuelle Version
Die aktuelle Version ist **3.0** vom 25.02.2024 (siehe [Neuigkeiten](#neuigkeiten)).  

## Anwendung
Voraussetzung für die Ausführung des Scripts ist eine lauffähige **PowerShell** Umgebung.  
Microsoft Windows (10/11) Besitzer haben es einfach, da ist PowerShell automatisch integriert. Um PowerShell unter Linux oder macOS zu installieren, bietet Microsoft Hilfe an unter <a href="https://aka.ms/pscore6" target="_blank">https://aka.ms/pscore6</a>.  
Die folgende Anleitung bezieht sich auf eine Ausführung unter Microsoft Windows. Die Ausführung auf anderen Betriebssystemen kann variieren; z.B. lautet der Befehl `PowerShell` unter macOS `pwsh`. Weitere Informationen zur Ausführung unter macOS liefert <a href="https://threema-forum.de/start/index.php?user/5482-curve/" target="_blank">Curve</a> im Threema-Forum <a href="https://threema-forum.de/index.php?thread/5505-chat-aus-daten-backup-in-html-konvertieren/&postID=74868#post74868" target="_blank">hier</a>.  

Benötigt wird ein Threema **Android Daten-Backup** *(es werden nur **Android** Daten-Backups unterstützt!)*.  
1. das Daten-Backup in ein Verzeichnis entpacken. Da Threema das Backup mit moderner Verschlüsselungstechnologie (AES) schützt, lässt es sich nicht mit Bordmitteln entpacken. Benötigt wird ein externes Tool wie z.B. 7-Zip (<a href="https://7-zip.de/" target="_blank">7-zip.de</a> oder <a href="https://7-zip.org/" target="_blank">7-zip.org</a>). Das Backup hat ein Passwort!
2. das hier verfügbare Script herunterladen und in das oben erstellte Verzeichnis kopieren.
3. in dem oben erstellen Verzeichnis ein Kommandozeilen-Fenster öffnen (z.B. mit Shift + Rechtsklick auf eine freie Stelle im Verzeichnis -> PowerShell-Fenster hier öffnen) oder in einer Eingabeaufforderung (cmd) in das Verzeichnis wechseln.
4. optional: Datei bestimmen, welche die Chat-Nachrichten enthält:  
    `PowerShell -ExecutionPolicy Bypass -File .\threema-csv2html.ps1 ListContacts` zeige alle Kontakte und Dateinamen  
      
    `PowerShell -ExecutionPolicy Bypass -File .\threema-csv2html.ps1 ListGroups` zeige alle Gruppen und Dateinamen
5. konvertieren des/der Chats mit folgendem Befehl:  
  
    `PowerShell -ExecutionPolicy Bypass -File .\threema-csv2html.ps1 .\<chat.csv> [Optionen]`  

    wobei `<chat.csv>` entweder ein zu konvertierender Chat (z.B. `message_1234567890.csv`) oder eine Auswahl von Chat-Dateien (z.B. `message*.csv`, `group*.csv`, `*.csv`) ist. Muss mit `.csv` enden.  
    Mögliche Optionen:  
    `-myOwnName "<Name>"` wenn \<Name\> anstatt "Ich" als eigenen Absender ausgegeben werden soll  
    `-myOwnThreemaID "<ID>"` wenn \<ID\> als eigene Threema-ID ausgegeben werden soll  
    `-FirstDate "<Date>"` und `-LastDate "<Date>"` wenn der Zeitrahmen eingeschränkt werden soll. Sie können zusammen oder einzeln benutzt werden.  
    `-ShowThreemaIDs` wenn die Threema-IDs aller Absender ausgegeben werden sollen  
    `-ShowAllIdentities` wenn Absender bei allen Nachrichten (auch im 1:1-Chat) ausgegeben werden sollen  
    `-LinkQuotes` wenn mit Klick auf Zitat-Nachricht zu der zitierten Nachricht gesprungen werden soll (experimentell!)  
    `-RealFilenames` wenn die Medien-Dateien mit ihren echten Dateinamen bzw. -suffixe ausgegeben werden sollen  
    `-WebStyle` wenn die HTML-Datei im modernen an Threema Web angelehnten Stil erstellt werden soll  
    `-DarkMode` wenn die HTML-Datei mit dunklen Farben erstellt werden soll  
    `-CSS "<File>"` wenn, anstatt eine interne, eine externe Format-Vorlage (*.css) verwendet werden soll  
    Weitere Informationen und Beispiele siehe unten bei [Weitere Informationen](#weitere-informationen).

Das Script erstellt einen Medienordner sowie eine .html-Datei.  
Die .html-Datei kann mit jedem beliebigen Browser geöffnet werden und darf nachträglich umbenannt werden.  
In dem Medienordner verschiebt das Script alle nötigen Dateien (die angegebene .csv-Datei, Bilder, Videos, Sprachnachrichten, ...) und **darf nicht** umbenannt werden!  
Auch hierzu gibt es [Weitere Informationen](#weitere-informationen).  

Der Befehl  
`PowerShell -ExecutionPolicy Bypass -File .\threema-csv2html.ps1 Help`  
zeigt alle verfügbaren Befehle und Optionen an.


## Neuigkeiten
- Version 3.0
    - Implementierung "Web-Ansicht" (hauptsächlich CSS Änderungen)
    - Möglichkeit hinzugefügt, externe CSS zu benutzen (und zwei Beispiel-CSS inkl. Threema Bildern im Ordner `lib` erstellt)
    - Laufzeitoptimierung: besonders größere Chats werden nun deutlich schneller verarbeitet
    - Option "RealFilenames" wird nun auch bei Gruppen-/Kontakt-/Kanal-Fotos berücksichtigt
    - bugfix: Links in "alten Zitaten" nicht mehr zerschießen, wenn nach dem Link ein Leerzeichen folgt ("> ")
    - das "Change Log" aus dem Script entfernt und in externe Text-Datei `changelog.txt` verschoben
    - Credits in die Hilfe-Ausgabe hinzugefügt
- Version 2.3.2
    - Verarbeitung von "alten" Zitaten etwas angepasst (weniger WhiteSpace)
    - Verarbeitung von Umfragen etwas angepasst (weniger WhiteSpace)
    - Avatare und Gruppenbilder werden nun auch (nur noch) kopiert und die Suche danach ausgeweitet
    - Doppelpunkte hinter den Namen wegrationalisiert
    - Namenssuche leicht optimiert (identity_id vor identity)
    - bugfix: "VOIP_STATUS" korrigiert (Tabelle besser eingebunden)
- Version 2.3.1
    - bugfix: PowerShell das Auswerten von Platzhaltern beim Speichern ausgetrieben
    - bugfix: zeige korrekte Anzahl der konvertierten Datensätze an (betrifft eingeschränkte Zeiträume)
- Version 2.3
    - neue Option `RealFilenames` - damit wird versucht, jede Medien-Datei mit dem richtigen Dateinamen (oder zumindest -suffix) abzuspeichern
    - Möglichkeit hinzugefügt, mehrere Chats gleichzeitig zu konvertieren (erlaubt Platzhalter '*' und '?')
    - Textausgaben angepasst, um nur nötige Informationen auszugeben
- Version 2.2
    - neue Optionen `FirstDate` und `LastDate` - damit kann der Zeitrahmen eingeschränkt werden
    - neue Option `DarkMode` - erzeugt die HTML-Datei mit dunklen Farben. Mein Versuch als Nicht-Designer ;)
    - bugfix: Dateiname - "CleanUp" regex nochmals angepasst. Jetzt sollte es passen.
    - HTML: \<DOCTYPE\> hinzugefügt und dafür css korrigiert
    - HTML: einige "div"-Elemente vereinfacht
    - HTML: einige "style"-Elemente hinzugefügt/angepasst
- Version 2.1.1
    - bugfix: Dateiname - "CleanUp" hatte Fehler im regex (und ? wurde auch nicht erwischt)
    - bugfix: alte Backups - da waren die Datensätze noch nicht so breit. Nutzen, was da ist und ignorieren, was fehlt ;)
    - kleinere Optimierungen - ".g_msg_states", "LinkQuotes"
- Version 2.1
    - VoIP-Anrufe in 1:1-Chats hinzugefügt
    - Gruppenanrufe hinzugefügt
    - nach Erstellung der HTML-Datei wird die entsprechende *.csv-Datei in den Medienordner **kopiert** (und nicht mehr verschoben)
- Version 2.0
    - Message-Stati (Daumen hoch, Daumen runter) bei Gruppen-Chats hinzugefügt
    - zwei neue Befehle (`ListContacts` und `ListGroups`) hinzugefügt, um die Dateinamen der Chats zu bestimmen
    - Parameterübergabe für benutzerdefinierte Optionen (yeah!)
    - zwei benutzerdefinierte Optionen in eine neue zusammengelegt und eine umbenannt
         - `ShowAllIdentities` wenn Absender bei allen Nachrichten (auch im 1:1-Chat) ausgegeben werden sollen. Ersetzt zwei Optionen, die in v1.3 eingeführt wurden.
         - `LinkQuotes` wenn mit Klick auf Zitat-Nachricht zu der zitierten Nachricht gesprungen werden soll (experimentell!). Ersetzt eine Option, die in v1.4 eingeführt wurde.
    - Hilfe-Funktion und -Text hinzugefügt
    - bugfix: Links in Zitaten nicht mehr doppelt umwandeln
    - bugfix: Umwandlung in echte Links: \s hat gefehlt
    - bugfix: "VOICEMESSAGE" ist zwar alt, hat aber gefehlt
- Version 1.4
    - neue benutzerdefinierte Option hinzugefügt <del>(muss z.Zt. noch direkt in Script angepasst werden, falls gewünscht)</del>:
        - <del>`JumpQuotes` wenn mit Klick auf Zitat-Nachricht zu der zitierten Nachricht gesprungen werden soll (experimentell! - Default: `Nein`)</del>
    - URLs in Nachrichten werden nun als klickbare Links ausgegeben
    - Umfrage-Beginn und -Ende haben nun auch einen Absender
- Version 1.3
    - benutzerdefinierte Optionen hinzugefügt <del>(müssen z.Zt. noch direkt in Script angepasst werden, falls gewünscht)</del>:
        - `myOwnName` der Name, der für einen selbst ausgegeben werden soll (Default: `"Ich"`)
        - `myOwnThreemaID` die Threema-ID, die für einen selbst ausgegeben werden soll (Default: `<keine>`)
        - `ShowThreemaIDs` wenn neben den Namen auch die Threema-IDs ausgegeben werden sollen (Default: `Nein`)
        - <del>`ShowOwnIdentityInGroups` wenn in Gruppen-Chats auch bei den eigenen Nachrichten der Absender ausgegeben werden soll (Default: `Nein`)</del>
        - <del>`ShowIdentitiesInSingleChats` wenn auch in 1:1-Chats Absender ausgegeben werden sollen (Default: `Nein`)</del>
        <!-- -->
        Hierzu gibt es natürlich auch [Weitere Informationen](#weitere-informationen).
    - wenn Nachricht/Zitat/Datei nicht vorhanden, gebe ID/Name des fehlenden Objekts aus
    - um die Zeit-Konsistenz beizubehalten, wird "created_at" anstatt "posted_at" als Timestamp benutzt (passt besser, besonders bei Umfragen)
    - wenn Nachricht einen Geändert-Timestamp besitzt, diese Info mit ausgeben
    - benutze Timestamp als MouseOver-Text bei Status-Nachrichten
    - bei Zitaten immer einen Absender benutzen
    - kleine Formatierungs- und Anzeige-Korrekturen bei Umfragen
    - allgemein ein paar Änderungen am Nachrichten-Design in der HTML-Datei
    - noch mehr Anzeigen bei Scriptausführung (u.a. eine Progress-Bar!)
    - Thumbnail-Datei wird nun nicht mehr gelöscht, wenn die originale Datei gefunden wurde
- Version 1.2.2
    - Script Dateiformat geändert auf UTF-8-BOM (zeigt in der Ausgabe (wieder?) Umlaute korrekt an)
- Version 1.2.1
    - optische Kosmetik
- Version 1.2
    - Implementierung der Zitat-Funktion nach dem neuen Threema Datenbankformat (mittels Referenz-ID)
- Version 1.1.1
    - neue Text-Ausgaben bei Scriptausführung (Version, Anzahl Datensätze)
- Version 1.1
    - Chatpartner bzw. Gruppenname wird nun wieder korrekt ermittelt und angezeigt
    - Namensfindungscode etwas optimiert
- Version 1.0.1
    - Fehler bei manchen Zitaten (nach dem alten System) behoben
- Version 1.0.0  
    - ich habe die letzte Version von jnL vom 21.01.2022 der Einfachheit halber als v1.0.0 definiert


## Weitere Informationen
- die Chat-Dateien, die in dem entpackten Backup zu finden sind, haben zur Zeit folgendes Format:  
    für 1:1-Chats: `message_<10-stellige ID>.csv`  
    für Gruppen-Chats: `group_message_<10-stellige ID>.csv`  
  
    Welche ID zu welchem Chat gehört, kann  
    für Kontakte mit: `PowerShell -ExecutionPolicy Bypass -File .\threema-csv2html.ps1 ListContacts`  
    und für Gruppen mit: `PowerShell -ExecutionPolicy Bypass -File .\threema-csv2html.ps1 ListGroups`  
    herausgefunden werden.
- benutzerdefinierte Optionen  
    Die Optionen werden per Parameter dem Script übergeben, wenn gewünscht. Keine davon ist zwingend notwendig.  
    Ohne Angabe von Optionen benutzt das Script die Standard-Ansicht von Threema: Bei Gruppen-Chats werden Namen nur bei empfangenen Nachrichten ausgegeben und bei 1:1-Chats gar keine Namen.  
    Beispiel 1:1-Chat:  
    ![Beispiel1](examples/Image1.png "Beispiel1")  
    Beispiel Gruppenchat:  
    ![Beispiel2](examples/Image2.png "Beispiel2")  
    Mit den Kombination `-myOwnName "<Name>" -ShowAllIdentities` wird in Gruppen-Chats zusätzlich bei gesendeten Nachrichten der Name ausgegeben und bei 1:1-Chats alle Namen. \<Name\> mit dem eigenen Namen ersetzen!  
    Beispiel:  
    ![Beispiel3](examples/Image3.png "Beispiel3")  
    Mit der Kombination `-myOwnThreemaID "<ID>" -ShowThreemaIDs` werden zusätzlich zu den Namen noch die Threema-IDs mit ausgegeben. \<ID\> mit der eigenen Threema-ID ersetzen!  
    Beispiel:  
    ![Beispiel4](examples/Image4.png "Beispiel4")  
      
    `-FirstDate "<Date>"` und `-LastDate "<Date>"` können einzeln oder zusammen benutzt werden.  
    Diese Optionen können benutzt werden, um den Start/das Ende/den Zeitraum für die Konvertierung zu bestimmen. Mit nur `-LastDate "<Date>"` kann man z.B. ältere Zeitabschnitte ausgeben, bevor man sie in der App löscht ;) &nbsp; Als `<Date>` ein Datum mit Tag, Monat und Jahr angeben, z.B. "30.01.2017".  
      
    `-LinkQuotes` kann bei allen Kombinationen mit übergeben werden, damit zitierte Nachrichten "anklickbar" werden, um zu der zitierten Nachricht zu springen.  
      
    `-RealFilenames` kann benutzt werden, damit die Medien-Dateien mit ihren echten Dateinamen (falls vorhanden) bzw. wichtiger: ihren Dateiendungen (Suffix) abgespeichert werden. Damit werden Bilder, Sprachnachrichten, Videos usw. deutlicher als solche gekennzeichnet und lassen sich direkt (z.B. im Dateiexplorer) aufrufen und öffnen.  
    **Achtung:** leider sind die Dateinamen, die das Threema-Export bereitstellt, nicht einmalig. Soll heissen, das Abspeichern von Medien-Dateien nur mit ihren echten Dateinamen kann das Überschreiben bereits vorhandener Dateien erwirken! Um das zu verhindern, und um die Dateinamen weiterhin eindeutig zu halten, wird die eindeutige Datei-ID vorangestellt, mit einem Trenner (z.Zt. `"__"`) vom Dateinamen getrennt, und so abgespeichert. So wird z.B. aus einer Sprachnachricht namens `recordAudio.m4a` die Datei `message_media_<36-stellige ID>__recordAudio.m4a`.   
      
    `-WebStyle` kann benutzt werden, um die HTML-Datei mit einem moderneren, an Threema Web angelehnten Stil zu erstellen. Inklusive Kontakt-Details! Beispiel:  
    ![Beispiel5](examples/Image5.png "Beispiel5")  
    Hier ist zu sehen, daß es keinerlei Hintergründe/-bilder gibt. Das ist soweit gewollt, um die Benutzung des Scripts weiterhin einfach zu halten, und man keine externen Dateien (wie eben Bilder) benötigt und hin-und-her kopieren muss. Für den, der es dennoch schicker hätte oder sich die Format-Vorlage selber anpassen möchte, gibt es zusätzlich die Option:  
    `-CSS "<File>"` kann benutzt werden um, anstatt eine der im Script eingebauten, eine externe Format-Vorlage zu verwenden. Als Beispiel stehen im Ordner `lib` zwei CSS-Dateien sowie die von Threema benutzen Hintergründe/-bilder bereit. Mit Benutzung von `-CSS "lib/web_light.css"` sieht das obige Beispiel so aus:  
    ![Beispiel6](examples/Image6.png "Beispiel6")  
    Diese Option bietet z.B. auch die Möglichkeit, bei mehreren Exporten auf eine einzige Format-Vorlage zu verweisen und somit z.B. eigene Änderungen gleichzeitig auf alle Chats, die auf diese Format-Vorlage verweisen, anzuwenden.  

    `-DarkMode` kann bei allen Kombinationen (inklusive `-WebStyle`) mit übergeben werden, um die Farben für Text und Hintergrund "dunkel" auszurichten. Meine Interpretation eines dunklen Themas.
- bis auf "Help" können alle Befehle und Optionen verkürzt übergeben werden, solange sie eindeutig bleiben:  
    z.B. "listc" für "ListContacts", "listg" für "ListGroups", "-showt" für "-ShowThreemaIDs", "-showa" für "ShowAllIdentities", "-link" für "-LinkQuotes", "-web" für "-WebStyle" usw.  
    Und wer es noch nicht bemerkt hat: Gross-/Kleinschreibung wird ignoriert.  
- der Medienordner, der bei erfolgreicher Ausführung erstellt wurde, liegt im Backup-Verzeichnis und heißt  
    für 1:1-Chats: `id_<10-stellige ID>`  
    für Gruppen-Chats: `gc_<10-stellige ID>`
- die erstellte HTML-Datei liegt ebenfalls im Backup-Order und heißt  
    für 1:1-Chats: `Chat mit <Threema-Name> (<Threema-ID>) <Datum und Uhrzeit der Erstellung>.html`  
    für Gruppen-Chats: `Chatgruppe <Name der Gruppe> <Datum und Uhrzeit der Erstellung>.html`

- dieses Script beruht auf der Vorarbeit von <a href="https://threema-forum.de/start/index.php?user/2762-jnl/" target="_blank">jnL</a>, der im April 2020 im <a href="https://threema-forum.de/" target="_blank">threema-forum.de</a> netterweise sein Script der Allgemeinheit zur Verfügung stellte und weiter entwickelte. Ab November 2023 beteilige ich mich an seiner Absicht, entwickle es weiter und hoste das Script auf Codeberg. Im Januar 2024 hat <a href="https://threema-forum.de/start/index.php?user/5482-curve/" target="_blank">Curve</a> die an Threema Web angelehnte Format-Vorlage beigesteuert. Das Script wird sicherlich noch weiter angepasst - da aber auch ich (wer nicht?) nur begrenzte Zeit habe, werde ich keine gesicherte Weiterentwicklung anbieten können.

