Hallo interessierte Threema-Nutzer,

in Anlehnung an *Miaz* Vorschlag [Export von Chats im Original-Layout mit Vorschaugrafiken der Medien als PDF oder HTML](https://threema-forum.de/start/vorschlag/index.php?suggestion/3)
habe ich mich mal daran versucht, ein **PowerShell-Skript** zu erstellen, welches aus einem Chat (Einzel-, Gruppenchat, Kanal) aus dem Android-Daten-Backup eine HTML erzeugt, die euch ermöglicht, den Chat fast so lesen zu können, wie er in der App dargestellt wird.

Dazu ganz einfach das .zip Daten-Backup auf dem Windows-Rechner in einem Ordner entpacken, in den ihr die .ps1 Skriptdatei kopiert.

Anschließend ruft ihr in diesem Verzeichnis eine Kommandozeilen-Fenster auf (Shift + Rechtsklick auf eine freie Stelle im Ordner -> PowerShell-Fenster/cmd hier öffnen) und gebt folgenden Befehl ein:

```bash
PowerShell -ExecutionPolicy Bypass -File .\<skript.ps1> .\<chat.csv>
```

Als Beispiel:

```bash
PowerShell -ExecutionPolicy Bypass -File .\_threema-csv2html.ps1 .\message_ECHOECHO.csv
```

**Das Skript** liest alle benötigten Informationen aus den hoffentlich vorhandenen Dateien des Backups und **erstellt einen Medienordner** (für Bilder, Videos, ... und die angegebene .csv Datei), **sowie eine .html Datei**, die mit jedem beliebigen Browser angesehen werden kann. Diese .html Datei könnt ihr umbenennen, den Ordner nicht, da die Verweise sonst nicht mehr stimmen.

*Derzeit unterstützt das Skript nur Android-Threema-Nutzer (Daten-Backup) mit einem Windows System (PowerShell).*

Probiert es aus, lest euch das Skript durch, wenn ihr mögt (oder mir nicht vertraut :) ) und bringt gerne Feedback oder eigene Verbesserungen ein.

Portierungen auf Linux/MacOS sind gerne gesehen und wenn ihr das Skript teilt, verweist bitte wenigstens auf [diesen Beitrag](https://threema-forum.de/index.php?thread/5505-chat-aus-daten-backup-in-html-konvertieren/) hier im Forum ;)

Viele Spaß damit!

PS: Nach dem Herunterladen der Skriptdatei aus dem Anhang müsst ihr die Dateiendung ".txt" entfernen, sodass die Datei auf ".ps1" endet, da es sonst nicht von PowerShell ausgeführt wird. Lasst ihr die Dateiendung auf ".txt" stehen, könnt ihr per Doppelklick den Inhalt des Skripts einsehen, es wird auch nicht ausgeführt.
  
  
**Fehler bei manchen Zitaten**

Die Zeile 178 endet aktuell auf `$tmp_content_from_user`   
muss aber korrekterweise mit `$msg_content_split[$i]+"<br/>"` enden.

Also einfach anpassen, dann sollte auch die erste Textzeile nach dem 1-Zeiler-Zitat korrekt in die HTML übernommen werden.


*Hinweis:*

Ich stelle das Skript hier nur einmal als "as is" hin, biete also keinen aktiven Support oder eine gesicherte Weiterentwicklung dafür an.



20.04.2020 jnL
